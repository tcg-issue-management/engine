package com.protoss.ims.bot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class UserLine {


	private @Id
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	private String realId;
	private String displayName; 

}
