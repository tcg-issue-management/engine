package com.protoss.ims.bot.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Priority {


	private @Id
	@GeneratedValue(strategy= GenerationType.TABLE) Long id;
	private @Version
	@JsonIgnore
	Long version;
	private String hasText;
	private String flagActive;
	private Integer value;  /* 0 = Critical, 1 = Error, 2 = Warning, 3 = Normal */
	private String responseType;
	private String responseValue;
	private String groupLineRealId;

}
