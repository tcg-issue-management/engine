package com.protoss.ims.bot.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.protoss.ims.bot.entity.UserLine;
import com.protoss.ims.bot.repository.UserLineRepository;

@Service
public class UserLineServiceImp implements UserLineService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserLineRepository userLineRepository;


    @Transactional
    @Override
    public Long insertUserLine(UserLine obj) {
        try{
            List<UserLine> groupLines = userLineRepository.findByRealId(obj.getRealId());

            if(groupLines.size() == 0){
            	userLineRepository.saveAndFlush(obj);
            	
            }else{
            	groupLines.get(0).setDisplayName(obj.getDisplayName());
            }

            return obj.getId();
        }catch (Exception e){
            LOGGER.error(" ===- Exception[insertUserLine] Msg : [{}]",e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}
