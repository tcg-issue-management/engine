package com.protoss.ims.bot.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.protoss.ims.bot.entity.Cards;
import com.protoss.ims.bot.entity.GroupLine;
import com.protoss.ims.bot.entity.ProjectGroup;
import com.protoss.ims.bot.repository.CardsRepository;
import com.protoss.ims.bot.repository.GroupLineRepository;
import com.protoss.ims.bot.repository.ProjectGroupRepository;

@Service
public class CardsServiceImp implements CardsService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CardsRepository cardsRepository;

    @Autowired
    private GroupLineRepository groupLineRepository;
    
    @Autowired
    private ProjectGroupRepository projectGroupRepository;

    @Transactional
    @Override
    public Long insertCards(Cards card,String groupId) {
        try{
        	
        	ProjectGroup projectGroup = projectGroupRepository.findFirstByRealId(groupId);
        	if(projectGroup!=null){
        		List<GroupLine> groupLineList = groupLineRepository.findByRealId(card.getGroupLine());
                if(groupLineList.size() > 0){
                	card.setGroupLine(groupLineList.get(0).getDisplayName());
                }
                card.setProjectGroup(projectGroup.getId());
                cardsRepository.saveAndFlush(card);
        	}
            return card.getId();
        }catch (Exception e){
                LOGGER.error(" ===- Exception[insertCards] Msg : [{}]",e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
        }


    }
}
