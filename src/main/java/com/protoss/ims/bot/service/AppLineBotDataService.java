package com.protoss.ims.bot.service;

import java.util.List;

import com.protoss.ims.bot.entity.MasterDataDetail;

public interface AppLineBotDataService {

    List<MasterDataDetail> masterDatakey(Long id, String code);
    boolean checkTextMatches(String str);
    String checkText(String str);
    String checkPriorityMessage(String str);

}
