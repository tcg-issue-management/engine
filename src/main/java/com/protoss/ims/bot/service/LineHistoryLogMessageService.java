package com.protoss.ims.bot.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.protoss.ims.bot.entity.LineHistoryLogMessage;
import com.protoss.ims.bot.entity.LineLogMessage;
import com.protoss.ims.bot.entity.MasterDataDetail;

public interface LineHistoryLogMessageService {

    Long insertLineHistoryLogMessage(LineHistoryLogMessage obj);
    Boolean findLastLineHistoryLogMessage(String groupLine,String sender);

}
