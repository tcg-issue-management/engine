package com.protoss.ims.bot.service;

import org.springframework.data.repository.query.Param;

import com.protoss.ims.bot.entity.GroupLine;
import com.protoss.ims.bot.entity.LineLogMessage;

public interface GroupLineService {



    Long insertGroupLine(GroupLine obj);
    void updateGroupLine(GroupLine obj);
    GroupLine findFirstByCode(@Param("code")String code);
    GroupLine findFirstByRealId(@Param("realId")String realId);

}
