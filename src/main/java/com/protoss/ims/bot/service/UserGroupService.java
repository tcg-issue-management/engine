package com.protoss.ims.bot.service;

import java.util.List;

import com.protoss.ims.bot.entity.UserGroup;
import com.protoss.ims.bot.entity.UserLine;

public interface UserGroupService {

	List<UserGroup> findByGroupIdAndUserIdAndRoleAccess(String groupId,String userId,String roleAccess);
	UserGroup findByGroupIdAndUserId(String groupId,String userId);
	Long insertUserLine(UserGroup userGroup);

}
