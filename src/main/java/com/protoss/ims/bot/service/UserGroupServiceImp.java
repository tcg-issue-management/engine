package com.protoss.ims.bot.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.protoss.ims.bot.controller.LineBotController;
import com.protoss.ims.bot.entity.UserGroup;
import com.protoss.ims.bot.repository.UserGroupRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserGroupServiceImp implements UserGroupService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserGroupRepository userGroupRepository;

	@Override
	public List<UserGroup> findByGroupIdAndUserIdAndRoleAccess(String groupId, String userId, String roleAccess) {
		// TODO Auto-generated method stub
		return userGroupRepository.findByGroupIdAndUserIdAndRoleAccess(groupId, userId, roleAccess);
	}

	@Override
	public UserGroup findByGroupIdAndUserId(String groupId, String userId) {
		// TODO Auto-generated method stub
		return userGroupRepository.findFirstByGroupIdAndUserId(groupId, userId);
	}

	@Transactional
	@Override
	public Long insertUserLine(UserGroup userGroup) {
		// TODO Auto-generated method stub
		UserGroup userGroupModel = userGroupRepository.findFirstByGroupIdAndUserId(userGroup.getGroupId(), userGroup.getUserId());
		log.info("userGroupModel={}",userGroupModel);
		if(userGroupModel != null){
			log.info("CASE UPDATE");
			userGroupModel.setUserName(userGroup.getUserName());
			userGroupRepository.saveAndFlush(userGroupModel);
		}else{
			log.info("CASE INSERT");
			userGroupModel = userGroupRepository.saveAndFlush(userGroup);
		}
		return userGroupModel.getId();
	}


    
}
