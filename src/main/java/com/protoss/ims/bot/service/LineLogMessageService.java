package com.protoss.ims.bot.service;

import java.util.List;

import com.protoss.ims.bot.entity.LineLogMessage;
import com.protoss.ims.bot.entity.MasterDataDetail;

public interface LineLogMessageService {

    Long insertLineLogMessage(LineLogMessage obj);

}
