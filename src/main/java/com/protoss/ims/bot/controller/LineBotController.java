package com.protoss.ims.bot.controller;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.LeaveEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.*;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.protoss.ims.bot.entity.Cards;
import com.protoss.ims.bot.entity.GroupLine;
import com.protoss.ims.bot.entity.IssueMessageWord;
import com.protoss.ims.bot.entity.LineHistoryLogMessage;
import com.protoss.ims.bot.entity.LineLogMessage;
import com.protoss.ims.bot.entity.Priority;
import com.protoss.ims.bot.entity.UserGroup;
import com.protoss.ims.bot.entity.UserLine;
import com.protoss.ims.bot.repository.IssueMessageWordRepository;
import com.protoss.ims.bot.repository.PriorityRepository;
import com.protoss.ims.bot.service.AppLineBotDataService;
import com.protoss.ims.bot.service.CardsService;
import com.protoss.ims.bot.service.GroupLineService;
import com.protoss.ims.bot.service.LineHistoryLogMessageService;
import com.protoss.ims.bot.service.LineLogMessageService;
import com.protoss.ims.bot.service.SpecialService;
import com.protoss.ims.bot.service.UserGroupService;
import com.protoss.ims.bot.service.UserLineService;
import com.protoss.ims.bot.util.ConstantVariable;
import com.protoss.ims.bot.util.CrcUtil;
import com.protoss.ims.bot.util.MethodUtil;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Slf4j
@LineMessageHandler
public class LineBotController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private AppLineBotDataService appLineBotDataService;

    @Autowired
    private LineLogMessageService lineLogMessageService;

    @Autowired
    private LineHistoryLogMessageService lineHistoryLogMessageService;
    
    @Autowired
    private SpecialService specialService;

    @Autowired
    private GroupLineService groupLineService;
    
    @Autowired
    private UserLineService userLineService;
    
    @Autowired
    private UserGroupService userGroupService;
    
    @Autowired
    private CardsService cardsService;



    public static List<Map<String,Object>> LIST_KEYWORD = new ArrayList<>();

    static String displayUserName = "";
    static String groupId = "";

    @EventMapping
    public void handleTextMessage(MessageEvent<TextMessageContent> event) throws IOException {

        handleTextContent(event.getReplyToken(), event, event.getMessage());

    }

    private void replyText(@NonNull  String replyToken, @NonNull String message) {
        if(replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken is not empty");
        }

        if(message.length() > 1000) {
            message = message.substring(0, 1000 - 2) + "...";
        }
        this.reply(replyToken, new TextMessage(message));
    }
    
    private void replyImage(@NonNull  String replyToken, @NonNull String message) {
        if(replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken is not empty");
        }
        this.reply(replyToken, new ImageMessage(message, message));
    }

    private void reply(@NonNull String replyToken, @NonNull Message message) {
        reply(replyToken, Collections.singletonList(message));
    }

    private void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
        try {
            BotApiResponse response = lineMessagingClient.replyMessage(
                    new ReplyMessage(replyToken, messages)
            ).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

    }


    private void handleTextContent(String replyToken, Event event, TextMessageContent content) throws IOException {
            log.info(" ===- In[handleTextContent] Msg : [{}] -===",content.getText());

            
            
            try{
                String text = content.getText();
                int priority = specialService.findHighPriorityMessage(text);
                String source = event.getSource().toString();
                Map mapLineSource = MethodUtil.findMapDataSourceLine(source);
                String roomId = (String) mapLineSource.get("roomId");
                String groupId = (String) mapLineSource.get("groupId");
                String eventType = "NONE";

                

                
                if(priority == 0){ // 
                	
                }else if(priority == 1){ // 
                	Map<String,Object>  map = specialService.findMessageResponse(text,groupId);
                	
                	if("AUTO_TEXT".equals(map.get("TYPE"))){
                		this.replyText(replyToken, String.valueOf(map.get("VALUE")));
                		eventType = "AUTO_TEXT";
                	}else if("AUTO_URL".equals(map.get("TYPE"))){
                		this.replyText(replyToken, String.valueOf(map.get("VALUE")));
                		eventType = "AUTO_URL";
                	}else if("AUTO_IMAGE".equals(map.get("TYPE"))){
                		this.replyImage(replyToken, String.valueOf(map.get("VALUE")));
                		eventType = "AUTO_IMAGE";
                	}
                	
                	
                	
                }else if(priority == 2){ //  Add Group
                	GroupLine groupLineObj = new GroupLine();
                    groupLineObj.setRealId(String.valueOf(mapLineSource.get("groupId")));
                    String realDisplayName = MethodUtil.seperateNameGroupLine(text);
                    groupLineObj.setDisplayName(realDisplayName == null ? String.valueOf(mapLineSource.get("userId")) : realDisplayName);
                    groupLineService.insertGroupLine(groupLineObj);
                    
                    GroupLine groupLine = groupLineService.findFirstByRealId(String.valueOf(mapLineSource.get("groupId")));
                    
                    String code = CrcUtil.genCode(""+groupLine.getId());
                    groupLine.setCode(code);
                    groupLineService.updateGroupLine(groupLine);
                    
                    this.replyText(replyToken, "Group Line Code :"+code);
                }else if(priority == 3){ // Add User
                	
                	String userId = String.valueOf(mapLineSource.get("userId"));
                	String realDisplayName = MethodUtil.seperateNameUserLine(text);
                	realDisplayName = realDisplayName == null ? null : realDisplayName;
                	
                	UserLine useLine = new UserLine();
                    useLine.setRealId(userId);
                    useLine.setDisplayName(realDisplayName);
                    userLineService.insertUserLine(useLine);
                    
                    UserGroup userGroup = userGroupService.findByGroupIdAndUserId(groupId, userId);
                    if(userGroup != null){
                    	userGroup.setUserName(realDisplayName);
                    }else{
                    	GroupLine groupLineObj = groupLineService.findFirstByRealId(groupId);
                    	
                    	userGroup = new UserGroup();
                    	userGroup.setGroupId(groupId);
                    	userGroup.setGroupName(groupLineObj.getDisplayName());
                    	userGroup.setUserId(userId);
                    	userGroup.setUserName(realDisplayName);
                    }
                    userGroupService.insertUserLine(userGroup);
                    
                }else if(priority == 4){ // Request Group Code
                	GroupLine groupLineObj = groupLineService.findFirstByRealId(String.valueOf(mapLineSource.get("groupId")));
                	this.replyText(replyToken, "Group Code :"+groupLineObj.getCode());
                }
                
                /* prepare object for insert*/
                String userId = event.getSource().getUserId();
                
                
                if("AUTO_TEXT".equals(eventType) && userId != null) {

                    lineMessagingClient.getProfile(userId)
                            .whenComplete((profile, throwable) -> {
                                    LineLogMessage lineLogMessage = new LineLogMessage();

                                    lineLogMessage.setMsg(text);
                                    lineLogMessage.setPriority(priority);
                                    lineLogMessage.setCreatedDate(MethodUtil.getCurrentDate());
                                    lineLogMessage.setGroupLine(String.valueOf(roomId  == null ? (groupId == null ? null : String.valueOf(groupId))  : String.valueOf(groupId)));
                                    //obj.setSender(profile != null  ? profile.getDisplayName() : userId);
                                    lineLogMessage.setSender(userId);
                                    

                                    /*insert Msg for DB*/
                                    List<UserGroup> adminUser = userGroupService.findByGroupIdAndUserIdAndRoleAccess(groupId, userId, "ADMIN");
                                    log.info(" ===- AdminUser : [{}] -===",adminUser);
                                    if(adminUser!=null && adminUser.size() > 0){
                                    	// In Case Admin
                                    }else{
                                    	
                                    	Boolean hasLastDataInTime = lineHistoryLogMessageService.findLastLineHistoryLogMessage(lineLogMessage.getGroupLine(), lineLogMessage.getSender());
                                    	hasLastDataInTime = false;
                                    	
                                    	if(!hasLastDataInTime){
                                        	log.info(" ===- [lineLogMessage={}] -===",lineLogMessage);
                                        	Long idIssue =   lineLogMessageService.insertLineLogMessage(lineLogMessage);
                                        	
                                        	log.info(" ===- idIssue={} -===",idIssue);
                                            if(idIssue != null){
                                            	log.info(" ===- [handleTextContent] Msg : [Save Issue Success] -===");
                                            	
                                            	Cards card = new  Cards();
                                            	card.setMsg(lineLogMessage.getMsg());
                                            	card.setPriority(lineLogMessage.getPriority());
                                            	card.setCreatedDate(lineLogMessage.getCreatedDate());
                                                card.setGroupLine(lineLogMessage.getGroupLine());
                                                card.setStatus(ConstantVariable.STATUS_CARD_OPEN);
                                            	card.setSender(lineLogMessage.getSender());
                                            	cardsService.insertCards(card,groupId);
                                            }
                                            else log.info(" ===- [handleTextContent] Msg : [Save Issue Fail] -===");
                                        }else{
                                        	log.info(" ===- Has Data in Time Limit -===");
                                        }
                                    }
                                    
                                    
                                    //Stamp log Every Case
                                    LineHistoryLogMessage obj = new LineHistoryLogMessage();

                                    obj.setMsg(text);
                                    obj.setPriority(priority);
                                    obj.setCreatedDate(MethodUtil.getCurrentDate());
                                    obj.setGroupLine(String.valueOf(roomId  == null ? (groupId == null ? null : String.valueOf(groupId))  : String.valueOf(groupId)));
                                    //obj.setSender(profile != null  ? profile.getDisplayName() : userId);
                                    obj.setSender(userId);

                                    /*insert Msg for DB*/
                                    lineHistoryLogMessageService.insertLineHistoryLogMessage(obj);
                                    
                                    
                            });


                }

                
                
//                //Stamp log Every Case
//                if(userId != null) {
//
//                    lineMessagingClient.getProfile(userId)
//                            .whenComplete((profile, throwable) -> {
//                                    LineHistoryLogMessage obj = new LineHistoryLogMessage();
//
//                                    obj.setMsg(text);
//                                    obj.setPriority(priority);
//                                    obj.setCreatedDate(MethodUtil.getCurrentDate());
//                                    obj.setGroupLine(String.valueOf(roomId  == null ? (groupId == null ? null : String.valueOf(groupId))  : String.valueOf(groupId)));
//                                    //obj.setSender(profile != null  ? profile.getDisplayName() : userId);
//                                    obj.setSender(userId);
//
//                                    /*insert Msg for DB*/
//                                    lineHistoryLogMessageService.insertLineHistoryLogMessage(obj);
//                                    
//                            });
//
//
//                }
                

                log.info(" ===- Out[handleTextContent] Msg : [{}] -===");
            }catch(Exception e){
                log.error(" ===- Exception[handleTextContent] Msg : [{}] -===",e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }

    }





}
