package com.protoss.ims.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.ims.bot.entity.LineHistoryLogMessage;
import com.protoss.ims.bot.entity.LineLogMessage;
import com.protoss.ims.bot.entity.MasterData;

public interface LineHistoryLogMessageRepository extends JpaSpecificationExecutor<LineHistoryLogMessage>,
        JpaRepository<LineHistoryLogMessage, Long>,
        PagingAndSortingRepository<LineHistoryLogMessage, Long> {


	LineHistoryLogMessage findTopByGroupLineAndSenderOrderByCreatedDateDesc(@Param("groupLine") String groupLine,@Param("sender") String sender);
}
