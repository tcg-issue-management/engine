package com.protoss.ims.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.ims.bot.entity.MasterData;

public interface MasterDataRepository extends JpaSpecificationExecutor<MasterData>,
        JpaRepository<MasterData, Long>,
        PagingAndSortingRepository<MasterData, Long> {


    MasterData findByCode(@Param("code") String code);

}
