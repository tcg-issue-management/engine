package com.protoss.ims.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.protoss.ims.bot.entity.LineLogMessage;
import com.protoss.ims.bot.entity.MasterData;

public interface LineLogMessageRepository extends JpaSpecificationExecutor<LineLogMessage>,
        JpaRepository<LineLogMessage, Long>,
        PagingAndSortingRepository<LineLogMessage, Long> {


}
