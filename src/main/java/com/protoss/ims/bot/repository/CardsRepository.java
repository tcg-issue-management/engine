package com.protoss.ims.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.protoss.ims.bot.entity.Cards;

public interface CardsRepository extends JpaSpecificationExecutor<Cards>,
        JpaRepository<Cards, Long>,
        PagingAndSortingRepository<Cards, Long> {


}
