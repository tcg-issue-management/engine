package com.protoss.ims.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.ims.bot.entity.GroupLine;
import com.protoss.ims.bot.entity.Priority;

public interface PriorityRepository extends JpaSpecificationExecutor<Priority>,
        JpaRepository<Priority, Long>,
        PagingAndSortingRepository<Priority, Long> {

	
}
