package com.protoss.ims.bot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.ims.bot.entity.UserLine;

public interface UserLineRepository extends JpaSpecificationExecutor<UserLine>,
        JpaRepository<UserLine, Long>,
        PagingAndSortingRepository<UserLine, Long> {



	List<UserLine> findByRealId(@Param("realId")String realId);

}
