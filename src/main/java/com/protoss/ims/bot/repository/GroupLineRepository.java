package com.protoss.ims.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.ims.bot.entity.GroupLine;
import com.protoss.ims.bot.entity.Priority;

import java.util.List;

public interface GroupLineRepository extends JpaSpecificationExecutor<GroupLine>,
        JpaRepository<GroupLine, Long>,
        PagingAndSortingRepository<GroupLine, Long> {


    List<GroupLine> findByRealId(@Param("realId")String realId);
    GroupLine findFirstByRealId(@Param("realId")String realId);
    GroupLine findFirstByCode(@Param("code")String code);


}
