package com.protoss.ims.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.protoss.ims.bot.entity.IssueMessageWord;

public interface IssueMessageWordRepository extends JpaSpecificationExecutor<IssueMessageWord>,
        JpaRepository<IssueMessageWord, Long>,
        PagingAndSortingRepository<IssueMessageWord, Long> {


}
